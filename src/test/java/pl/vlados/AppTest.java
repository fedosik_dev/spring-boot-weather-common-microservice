package pl.vlados;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import com.netflix.discovery.converters.Auto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.vlados.api.ForecastAccuweatherAPI;
import pl.vlados.api.LocationAccuweatherAPI;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

@RunWith(SpringRunner.class)
@WebMvcTest(SpringBootWeatherCommonMicroserviceController.class)
public class AppTest 
{
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ForecastAccuweatherAPI forecastAccuweatherAPI;

    @MockBean
    private LocationAccuweatherAPI locationAccuweatherAPI;

    @Test
    public void thisReturn200() throws Exception {

        mockMvc.perform(get("/weather/common/v1/5days/by/postal-code/{postal_code}", "03-287"))
                .andExpect(status().isOk());

    }
}
