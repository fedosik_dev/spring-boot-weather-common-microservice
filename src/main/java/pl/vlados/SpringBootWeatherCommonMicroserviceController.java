package pl.vlados;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.vlados.api.ForecastAccuweatherAPI;
import pl.vlados.api.LocationAccuweatherAPI;
import pl.vlados.dto.common.CommonWeatherDayItem;
import pl.vlados.dto.common.CommonWeatherResponse;
import pl.vlados.dto.forecast.Forecast5daysResponse;
import pl.vlados.dto.location.LocationResponseDto;

import java.util.stream.Collectors;

@RestController
@RequestMapping("weather/common/v1")
public class SpringBootWeatherCommonMicroserviceController {


    private static int countAccuweatherAPICalling = 0;

    @Autowired
    private LocationAccuweatherAPI locationAPI;

    @Autowired
    private ForecastAccuweatherAPI forecastAPI;

    @GetMapping("/5days/by/postal-code/{postal_code}")
    public CommonWeatherResponse getForecastFor5daysByPostalCode(@PathVariable(value = "postal_code") String postalCode) {

        CommonWeatherResponse response = null;

        if (!postalCode.isEmpty()) {
            LocationResponseDto locationByPostalCode = locationAPI.getLocationByPostalCode(postalCode);

            if (locationByPostalCode != null) {
                if (locationByPostalCode.getUseAccuweatherAPI()) {
                    countAccuweatherAPICalling++;
                }
                Forecast5daysResponse forecast5daysResponse = forecastAPI.get5daysForecast(locationByPostalCode.getLocationKey());
                if (forecast5daysResponse.getUseAccuweatherAPI()) {
                    countAccuweatherAPICalling++;
                }

                response = new CommonWeatherResponse(locationByPostalCode.getLocationPostalCode(),
                        locationByPostalCode.getLocationTitle(), locationByPostalCode.getLocationKey(),
                        forecast5daysResponse.getDayList().stream().map(CommonWeatherDayItem::parseFromDTO).collect(Collectors.toList()));
            }
        }

        return response;
    }

    @GetMapping("/accuweatherApiCallingCount")
    public String getCountCallingApi() {

        return "Total Accuweather api calling count is " + countAccuweatherAPICalling + " times";
    }

}
