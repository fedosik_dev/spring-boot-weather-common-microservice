package pl.vlados.dto.forecast;

import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
public class Forecast5daysResponse {

    private String port;
    private Boolean useAccuweatherAPI;
    private String locationKey;
    private String requestDate;
    private List<Day> dayList;
}
