package pl.vlados.dto.forecast;

import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@ToString
public class Day implements Serializable {
    private String date;
    private Double minTemp;
    private Double maxTemp;
}
