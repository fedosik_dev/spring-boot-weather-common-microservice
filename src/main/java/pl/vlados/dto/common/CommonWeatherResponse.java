package pl.vlados.dto.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class CommonWeatherResponse {

    String locationPostalCode;
    String locationTitle;
    String locationKey;

    List<CommonWeatherDayItem> days;
}
