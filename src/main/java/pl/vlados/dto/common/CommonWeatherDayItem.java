package pl.vlados.dto.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.vlados.dto.forecast.Day;

@Getter
@Setter
@Builder
public class CommonWeatherDayItem {
    String date;
    Double minTemp;
    Double maxTemp;

    public static CommonWeatherDayItem parseFromDTO(Day day) {
        return CommonWeatherDayItem.builder().date(day.getDate()).minTemp(day.getMinTemp()).maxTemp(day.getMaxTemp()).build();
    }
}
