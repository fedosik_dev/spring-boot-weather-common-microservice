package pl.vlados.dto.location;

import lombok.Getter;

@Getter
public class LocationResponseDto {

    private String port;
    private Boolean useAccuweatherAPI;
    private String locationKey;
    private String locationTitle;
    private String locationPostalCode;
}

