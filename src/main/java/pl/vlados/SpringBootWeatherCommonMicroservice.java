package pl.vlados;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("pl.vlados")
@EnableDiscoveryClient
public class SpringBootWeatherCommonMicroservice
{
    public static void main( String[] args )
    {
        SpringApplication.run(SpringBootWeatherCommonMicroservice.class, args);
    }
}
