package pl.vlados.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.vlados.dto.forecast.Forecast5daysResponse;

@FeignClient(value = "spring-boot-accuweather-forecast-service")
public interface ForecastAccuweatherAPI {

    @GetMapping("/forecast/v1/5days/{location_id}")
    Forecast5daysResponse get5daysForecast(@PathVariable(value = "location_id") String locationId);
}
