package pl.vlados.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.vlados.dto.location.LocationResponseDto;

@FeignClient(value = "spring-boot-accuweather-location-service")
public interface LocationAccuweatherAPI {

    @GetMapping("/location/v1/get/by/postal-code/{postal-code}")
    LocationResponseDto getLocationByPostalCode(@PathVariable(value = "postal-code") String postalCode);
}
